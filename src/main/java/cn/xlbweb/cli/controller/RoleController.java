package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.model.Role;
import cn.xlbweb.cli.server.ServerResponse;
import cn.xlbweb.cli.service.IRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestController
@RequestMapping("role")
public class RoleController {

    @Autowired
    private IRoleService iRoleService;

    @GetMapping("listRole")
    @RequiresPermissions("role:listRole")
    public ServerResponse listRole() {
        return iRoleService.listRole();
    }

    @PostMapping("insertRole")
    @RequiresPermissions("role:insertRole")
    public ServerResponse insertRole(Role role) {
        return iRoleService.insertRole(role);
    }

    @PutMapping("updateRole")
    @RequiresPermissions("role:updateRole")
    public ServerResponse updateRole(Role role) {
        return iRoleService.updateRole(role);
    }

    @DeleteMapping("deleteRole")
    @RequiresPermissions("role:deleteRole")
    public ServerResponse deleteRole(String ids) {
        return iRoleService.deleteRole(ids);
    }
}
