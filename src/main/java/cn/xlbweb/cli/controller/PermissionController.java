package cn.xlbweb.cli.controller;

import cn.xlbweb.cli.model.Permission;
import cn.xlbweb.cli.server.ServerResponse;
import cn.xlbweb.cli.service.IPermissionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: bobi
 * @date: 2019-02-04 00:55
 * @description:
 */
@RestController
@RequestMapping("permission")
public class PermissionController {

    @Autowired
    private IPermissionService iPermissionService;

    @GetMapping("listPermission")
    @RequiresPermissions("permission:listPermission")
    public ServerResponse listPermission() {
        return iPermissionService.listPermission();
    }

    @PostMapping("insertPermission")
    @RequiresPermissions("permission:insertPermission")
    public ServerResponse insertPermission(Permission permission) {
        return iPermissionService.insertPermission(permission);
    }

    @PutMapping("updatePermission")
    @RequiresPermissions("permission:updatePermission")
    public ServerResponse updatePermission(Permission permission) {
        return iPermissionService.updatePermission(permission);
    }

    @DeleteMapping("deletePermission")
    @RequiresPermissions("permission:deletePermission")
    public ServerResponse deletePermission(String ids) {
        return iPermissionService.deletePermission(ids);
    }
}
